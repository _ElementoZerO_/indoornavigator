package valerio.magliozzi.indoornavigator;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import java.lang.Math;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.vecmath.Quat4d;
import Jama.Matrix;
import uk.me.berndporr.iirj.Butterworth;


public class PositionCalculatorService extends Service implements SensorEventListener {
    /*--------------------------PositionCalculatorService Variables-------------------------------*/
    static final int STATUS_RUNNING = 0;
    static final int GUI_UPDATE = 1;
    static final int WALKING_UPDATE = 3;
    protected static final int LEG_THRSHOLD_INACTIVITY = 58;//58
    public static final int LEG_MOVEMENT_NONE = 0;
    public static final int LEG_MOVEMENT_FORWARD = 1;
    public static final int LEG_MOVEMENT_BACKWARD = 2;
    protected static final double LEG_THRSHOLD_AMPLITUDE = 0.80d;//0.50
    static final double CIRC_EARTH = 40075.04d;
    public double accMagnitude = 0.0d;
    private int mLastActivity = 0;
    private int mInactivityCount = 0;
    public double mLastXYZ = 0;
    public double stepCount = 0;
    boolean isWalking = false;
    //String reading;
    Bundle bundleData, bundleMoving = new Bundle();
    ResultReceiver receiver;
    StringBuilder Guibuilder = new StringBuilder();
    private boolean initState = true;
    boolean fusedIsOn;
    boolean lowPassIsOn;
    boolean highPassIsOn_Acc, highPassIsOn_Speed, highPassIsOn_Displ;
    boolean linAccIsOn;
    /*---------------------------Sensors Initialization-------------------------------------------*/
    private SensorManager SensorMgr = null;
    private List<Integer> sensorList = new ArrayList<Integer>();
    float gravity[] = new float[3];
    double androidLinearAcc[] = new double[3];
    double stairAcc;
    //private float alpha = 0.9f;
    /*---------------------------Physics&Math Calculation Variables-------------------------------*/
    private float timestamp;
    private double accTimestamp;
    public static final int GYRO_TIME_CONSTANT = 30;//30
    public static final int ACC_TIME_COSTANT = 32;
    public static final int GUI_TIME_COSTANT = 20;
    public float FILTER_COEFFICIENT = 0.98f;//0.98f;
    public float LPF_COEFFICIENT = 0.50f;
    private Timer taskTimer = new Timer();
    // Create a constant to convert nanoseconds to seconds.
    private static final double NS2S = 1.0f / 1000000000.0d;
    public static final float EPSILON = 0.000000001f;
    double prevFiltredAcc[] = new double[3];
    double prev_androidLinearAcc[] = new double[3];
    double acc_dT = 0;// acc dT
    double Ax = 0.0d, Ay = 0.0d, Az = 0.0d;
    double prevAx = 0.0d, prevAy = 0.0d, prevAz = 0.0d;
    double Vx = 0.0d, Vy = 0.0d;
    double prevVx = 0d, prevVy;
    double Sx = 0.0d, Sy = 0.0d, Sz = 0.0d, totSxy = 0.0d;
    double lonSx, latSy, tempLat, prevLatSy, prevLonSx;
    double prevSx = 0.0d, prevSy = 0.0d;
    double bias[] = new double[3];
    double bias_dT = 0.0d;
    int meanCount = 0;
    int NUMBER_SAMPLE = 60;
    int stairCount = 0;
    double prevMeanAz = 0.0d;
    double cusum = 0.0d;
    boolean stairDetect = false;
    double maxAz = 0.0d, minAz = 0.0d;
    ArrayList<stair> stairHistory;
    /*ArrayDeque<Double>*/double AzMean;
    double meanAz, meanMinAz, AzMaxPeak = 0, AzMinPeak = 0;
    DecimalFormat df = new DecimalFormat("#");
    boolean isFirstCyrcle = true;

    private float[] gravityValues = new float[3];

    // angular speeds from gyro
    private double[] gyro = new double[3];

    // rotation matrix from gyro data
    private double[] gyroMatrix = new double[9];

    // orientation angles from gyro matrix
    private double[] gyroOrientation = new double[3];

    // magnetic field vector
    private double[] magnet = new double[3];

    // accelerometer vector
    private double[] accel = new double[3];
    // Low Pass Accelerometer AND if true linear acceleration d
    private double[] LP_filtredAcc = new double[3];

    // orientation angles from accel and magnet
    private double[] accMagOrientation = new double[3];

    // final orientation angles from sensor fusion
    private double[] fusedOrientation = new double[3];

    // accelerometer and magnetometer based rotation matrix
    private double[] rotationMatrix = new double[9];


    private static final String TAG = "PositionCalculatorServ";
    /*
    public PositionCalculatorService() {
        super(PositionCalculatorService.class.getName());
    }
    */
    int zeroCount = 0;
    int tempZeroCount = 0;
    int sampleCount = 0;
    boolean firstZero = true;
    /*******************************KALMAN FILTER**************************************************/
    //A - state transition matrix
    private Matrix A;
    //B - control input matrix
    private Matrix B;
    //H - measurement matrix
    private Matrix H;
    //Q - process noise covariance matrix (error in the process)
    private Matrix Q;
    //R - measurement noise covariance matrix (error in the measurement)
    private Matrix R;
    //previous position
    private Matrix last_P;
    //previous speed
    private Matrix last_x;
    //external unknows vector
    private Matrix u = new Matrix(new double[]{0d, 0d, 0d, 0d}, 4);
    double kSx, kSy, kSz, kVx, kVy;
    /* *************ADDITIONAL FILTER FOR OUTPUT***************************************************/
    private Kalman1dFilter mFiltersCascade[] = new Kalman1dFilter[3];

    Butterworth butterworthHPFAccel[] = new Butterworth[3];
    Butterworth butterworthHPFSpeed[] = new Butterworth[2];
    Butterworth butterworthHPFDispl[] = new Butterworth[2];

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");

        //System.gc();

        df.setMaximumFractionDigits(8);


        gyroOrientation[0] = 0.0d;
        gyroOrientation[1] = 0.0d;
        gyroOrientation[2] = 0.0d;

        // initialise gyroMatrix with identity matrix
        gyroMatrix[0] = 1.0d;
        gyroMatrix[1] = 0.0d;
        gyroMatrix[2] = 0.0d;
        gyroMatrix[3] = 0.0d;
        gyroMatrix[4] = 1.0d;
        gyroMatrix[5] = 0.0d;
        gyroMatrix[6] = 0.0d;
        gyroMatrix[7] = 0.0d;
        gyroMatrix[8] = 1.0d;

        // Get sensor manager on starting the service.
        SensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        initKalmanFilter();
        //prev q = 0.01d r = 0.0025d
        mFiltersCascade[0] = new Kalman1dFilter(1d, 1d, 0.01d, 0.0025d);
        mFiltersCascade[1] = new Kalman1dFilter(1d, 1d, 0.01d, 0.0025d);
        mFiltersCascade[2] = new Kalman1dFilter(1d, 1d, 0.01d, 0.0025d);

        /* STAIR QUEUE HISTORY */
        stairHistory = new ArrayList<>();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "onStartCommand");
        try {

            receiver = intent.getParcelableExtra("receiver");
            Bundle extras = intent.getExtras();
            //in seguito aggiungere try catch per evitare casini

            lonSx = extras.getDouble("startSx");
            latSy = extras.getDouble("startSy");

            Log.d(TAG, "startSx: "+ lonSx + " startSy: "+ latSy);

            linAccIsOn = extras.getBoolean("linAccIsOn");//intent.getParcelableExtra("TYPE_ACC");
            Log.d(TAG, "linAccIsOn: " + linAccIsOn);

            FILTER_COEFFICIENT = extras.getFloat("FILTER_COEFFICIENT");
            Log.d(TAG, "FILTER_COEFFICIENT: " + FILTER_COEFFICIENT);

            LPF_COEFFICIENT = extras.getFloat("LPF_COEFFICIENT");
            Log.d(TAG, "LPF_COEFFICIENT: " + LPF_COEFFICIENT);

            fusedIsOn = extras.getBoolean("fusedIsOn");
            Log.d(TAG, "fusedIsOn: " + fusedIsOn);

            lowPassIsOn = extras.getBoolean("lowPassIsOn");
            Log.d(TAG, "accLowPassIsFilter: " + lowPassIsOn);

            highPassIsOn_Acc = extras.getBoolean("highPassIsOn_Acc");
            Log.d(TAG, "highPassIsOn_Acc: " + highPassIsOn_Acc);

            highPassIsOn_Speed = extras.getBoolean("highPassIsOn_Speed");
            Log.d(TAG, "highPassIsOn_Speed: "+ highPassIsOn_Speed);

            highPassIsOn_Displ = extras.getBoolean("highPassIsOn_Displ");
            Log.d(TAG, "highPassIsOn_Displ: "+ highPassIsOn_Displ);

            bias[0] = extras.getDouble("bias_x");
            bias[1] = extras.getDouble("bias_y");
            bias[2] = extras.getDouble("bias_z");
            bias_dT = extras.getDouble("bias_dT");

        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        sensorList.add(Sensor.TYPE_ACCELEROMETER);
        sensorList.add(Sensor.TYPE_GYROSCOPE);
        sensorList.add(Sensor.TYPE_MAGNETIC_FIELD);
        sensorList.add(Sensor.TYPE_LINEAR_ACCELERATION);

        // Registering...

        for (Integer sensor : sensorList) {
            //SENSOR DELAY FASTEST NOT WORK ON NEXUS 5X
            SensorMgr.registerListener(this, SensorMgr.getDefaultSensor(sensor), SensorManager.SENSOR_DELAY_FASTEST, SensorManager.SENSOR_STATUS_ACCURACY_HIGH);
        }

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("PositionCalculatorService")
                .setTicker("Test")
                .setContentText("PositionCalculatorService is Running")
                //.setSmallIcon(R.mipmap.ic_launcher)
                //.setContent(notificationView)
                .setOngoing(true).build();


        startForeground(101,
                notification);

        // wait for one second until gyroscope and magnetometer/accelerometer
        // data is initialised then scedule the complementary filter task

        if (fusedIsOn) {
            taskTimer.scheduleAtFixedRate(new calculateFusedOrientationTask(),
                    1000, GYRO_TIME_CONSTANT);
        }

        if (lowPassIsOn) {
            taskTimer.scheduleAtFixedRate(new calculateLowPassTask(),
                    1000, ACC_TIME_COSTANT);
        }

        taskTimer.scheduleAtFixedRate(new GUI_Update(),
                1000, GUI_TIME_COSTANT);

        if(highPassIsOn_Acc) {

            //Log.d(TAG, "highPassIsOn if "+ highPassIsOn);

            butterworthHPFAccel[0] = new Butterworth();
            butterworthHPFAccel[1] = new Butterworth();
            butterworthHPFAccel[2] = new Butterworth();
            //7500
            butterworthHPFAccel[0].highPass(20, 100, 500);
            butterworthHPFAccel[1].highPass(20, 100, 500);
            butterworthHPFAccel[2].highPass(20, 100, 500);
        }

        if(highPassIsOn_Speed) {
            butterworthHPFSpeed[0] = new Butterworth();
            butterworthHPFSpeed[1] = new Butterworth();
            //2000
            butterworthHPFSpeed[0].highPass(20, 100, 500);
            butterworthHPFSpeed[1].highPass(20, 100, 500);
        }

        if(highPassIsOn_Displ) {
            butterworthHPFDispl[0] = new Butterworth();
            butterworthHPFDispl[1] = new Butterworth();
            //1200
            butterworthHPFDispl[0].highPass(20, 100, 300);
            butterworthHPFDispl[1].highPass(20, 100, 300);
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        SensorMgr.unregisterListener(this);
        taskTimer.cancel();
        System.gc();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        switch (sensorEvent.sensor.getType()) {

            case Sensor.TYPE_GYROSCOPE: {
                GyroCalculator(sensorEvent);
                break;
            }
            case Sensor.TYPE_ACCELEROMETER: {
                //System.arraycopy(sensorEvent.values, 0, accel, 0, 3);
                for (int i = 0; i < sensorEvent.values.length; i++) {

                    accel[i] = sensorEvent.values[i];
                }
                /* *********BIAS CORRECTION*****************************/
                accel = biasCorrection(accel, bias);
                AccMagCalculator(sensorEvent);

                //Step Detector acceleration filter
                accMagnitude = Math.sqrt(accel[0] * accel[0] + accel[1] * accel[1] + accel[2] * accel[2]);
                accMagnitude = Kalman1DSmooth(accMagnitude);

                //stair detection low pass and kalman
                stairAcc = Kalman1DSmooth(stairAcc);
                break;
            }
            case Sensor.TYPE_MAGNETIC_FIELD: {
                // copy new magnetometer data into magnet array
                //System.arraycopy(sensorEvent.values, 0, magnet, 0, 3);
                for (int i = 0; i < sensorEvent.values.length; i++) {

                    magnet[i] = sensorEvent.values[i];
                }
                break;
            }
            case Sensor.TYPE_LINEAR_ACCELERATION: {
                //System.arraycopy(sensorEvent.values, 0, androidLinearAcc, 0, 3);
                for (int i = 0; i < sensorEvent.values.length; i++) {

                    androidLinearAcc[i] = sensorEvent.values[i];
                }
                /* *********BIAS CORRECTION*****************************/
                androidLinearAcc = biasCorrection(androidLinearAcc, bias);

                /* *********ACCELERATION HIGH PASS FILTER********************/
                if(highPassIsOn_Acc) {
                    androidLinearAcc[0] = butterworthHPFAccel[0].filter(androidLinearAcc[0]);
                    androidLinearAcc[1] = butterworthHPFAccel[1].filter(androidLinearAcc[1]);
                    androidLinearAcc[2] = butterworthHPFAccel[2].filter(androidLinearAcc[2]);
                }
                /* *********dT CALCULATION******************************/
                acc_dT = (sensorEvent.timestamp - accTimestamp) * NS2S;
                accTimestamp = sensorEvent.timestamp;
                //androidLinearAcc = calculateMeanFilter(androidLinearAcc);
                break;
            }

        }

        if (sensorEvent.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {

            System.arraycopy(androidLinearAcc, 0, prev_androidLinearAcc, 0, 3);
            if (!isFirstCyrcle) {
                bundleData = XYDispEstimator(bundleData);
                if (bundleData != null) {
                    receiver.send(STATUS_RUNNING, bundleData);
                }
            } else {
                isFirstCyrcle = false;
            }
            receiver.send(WALKING_UPDATE, bundleMoving);
        }
    }

    private Bundle XYDispEstimator(Bundle bundleData/*, SensorEvent sensorEvent*/) {

        //System.out.println("dT:"+dT);
        double gyroData[] = new double[3];
        double accData[] = new double[3];

        //final float dT = (sensorEvent.timestamp - timestamp) * NS2S;
        //System.out.println("accdT:"+acc_dT);

        if (fusedIsOn) {
            //System.arraycopy(fusedOrientation, 0, gyroData, 0, 3);
            arrayCopy(fusedOrientation, gyroData);

        } else {
            //System.arraycopy(accMagOrientation, 0, gyroData, 0, 3);
            arrayCopy(accMagOrientation, gyroData);
        }


        if (lowPassIsOn && linAccIsOn) {
            //System.arraycopy(LP_filtredAcc, 0, accData, 0 ,3);
            arrayCopy(LP_filtredAcc, accData);
        }

        if (lowPassIsOn && !linAccIsOn) {
            //System.arraycopy(LP_filtredAcc, 0, accData, 0 ,3);
            arrayCopy(LP_filtredAcc, accData);
        }

        if (!lowPassIsOn && linAccIsOn) {
            //System.arraycopy(androidLinearAcc, 0, accData, 0 ,3);
            arrayCopy(androidLinearAcc, accData);
        }

        if (!lowPassIsOn && !linAccIsOn) {
            //System.arraycopy(accel, 0, accData, 0 ,3);
            arrayCopy(accel, accData);
        }

        /* ****************************************************************************************/
        //rad/s * s = rad * s / s = rad
        /*
        Ax = ((accData[0]*(float)Math.cos(gyroData[0]))+(accData[1]*(float)Math.sin(gyroData[0])));
        Ay = ((-accData[0]*(float)Math.sin(gyroData[0]))+(accData[1]*(float)Math.cos(gyroData[0])));
        Az = accData[2];
        */
        double orientedAcc[] = calculateEarthOrientedAcceleration(accData, gyroData);
        //double orientedAcc[] = calculateEarthOrientedAcceleration_Quaternion(accData, gyroData);

        Ax = (-1*orientedAcc[0]);
        Ay = (-1*orientedAcc[1]);
        Az = orientedAcc[2];

        //System.out.println("accMagnitude: "+accMagnitude);

        /*
        * accMagnitude calculated into TYPE_ACCELERATION
        * */
        // *********************************WALKING************************************************
        //Log.d(TAG, "Leg threshold:"+(accMagnitude - mLastXYZ));
        if (Math.abs(accMagnitude - mLastXYZ) > LEG_THRSHOLD_AMPLITUDE) {

            mInactivityCount = 0;
            int currentActivity = (accMagnitude > mLastXYZ) ? LEG_MOVEMENT_FORWARD : LEG_MOVEMENT_BACKWARD;
            if (currentActivity != mLastActivity) {
                mLastActivity = currentActivity;
                isWalking = true;
                bundleMoving.putBoolean("isWalking", isWalking);
                bundleMoving.putDouble("dT", acc_dT);
            }
        } else {
            if (mInactivityCount > LEG_THRSHOLD_INACTIVITY) {
                if (mLastActivity != LEG_MOVEMENT_NONE) {
                    mLastActivity = LEG_MOVEMENT_NONE;
                    isWalking = false;
                    bundleMoving.putBoolean("isWalking", isWalking);
                    bundleMoving.putDouble("dT", acc_dT);
                }
            } else {
                mInactivityCount++;
            }
        }
        mLastXYZ = accMagnitude;

        //System.out.println("isWalking: "+isWalking);
        if(isWalking) {

            prevAx = Ax;
            prevAy = Ay;
            prevAz = Az;
            //Integrale accelerazione e velocità
            //Vx += (Ax * acc_dT);

            //Trapezoid Rule numerical integration
            prevVx = Vx;
            Vx = (prevVx + ((prevAx + Ax)/2)*acc_dT);

            if(highPassIsOn_Speed) {
                Vx = butterworthHPFSpeed[0].filter(Vx);
            }
            //Sx = (Vx * acc_dT) + ((Ax * (acc_dT * acc_dT)) / 2);

            //Trapezoid Rule numerical integration
            //prevSx = Sx;
            Sx = (/*prevSx +*/ ((prevVx+Vx)/2)*acc_dT);
            if(highPassIsOn_Displ) {
                Sx = butterworthHPFDispl[0].filter(Sx);
            }
            //Sx = butterworthBP[0].filter(Sx);

            //Vy += (Ay * acc_dT);

            //Trapezoid Rule numerical integration
            prevVy = Vy;
            Vy = (prevVy + ((prevAy + Ay)/2)*acc_dT);
            if(highPassIsOn_Speed) {
                Vy = butterworthHPFSpeed[1].filter(Vy);
            }
            //Sy = (Vy * acc_dT) + ((Ay * (acc_dT * acc_dT)) / 2);

            //Trapezoid Rule numerical integration
            //prevSy = Sy;
            Sy = (/*prevSy +*/ ((prevVy+Vy)/2)*acc_dT);
            if(highPassIsOn_Displ) {
                Sy = butterworthHPFDispl[1].filter(Sy);
            }
            //Sy = butterworthBP[1].filter(Sy);
            /*
            if(stairDetect) {

                Log.d(TAG, "meanMaxAz:"
                        + (meanMaxAz+ " stairAcc: "+stairAcc));


                stairCount += tempZeroCount;

            }
            */
            //Log.d(TAG, "meanMaxAz: "+meanMaxAz+ " meanMinAz: "+meanMinAz);
            /*
            Vz += (Az * acc_dT);
            prevVz = Vz;
            //Trapezoid Rule numerical integration
            Vz = (prevVz + ((prevAz + Az)/2) * acc_dT);

            Sz = (Vz * acc_dT) + ((Az * (acc_dT * acc_dT))/2);
            */

            /* *************************************KALMAN******************************************/

            //update dt in matrix A
            A.set(0, 2, acc_dT);
            A.set(1, 3, acc_dT);
            //A.set(2, 5, acc_dT);
            /*
            //update speed H
            H.set(2,2, kVx);
            H.set(3,3, kVy);
            */
            Matrix measurement = new Matrix(new double[]{Sx, Sy, Vx, Vy}, 4);
            //external unknows vector
            //u.set(2, 0, bias[0]);
            //u.set(3 ,0, bias[1]);
            //*********************PREDICTION***********************************************************
            Matrix x = ((A.times(last_x)).plus(B.times(u)));

            Matrix P = ((A.times(last_P)).times(A.transpose())).plus(Q);

            //*********************CORRECTION***********************************************************
            Matrix S = ((H.times(P)).times(H.transpose())).plus(R);

            Matrix K = (P.times(H.transpose())).times(S.inverse());

            Matrix y = measurement.minus(H.times(x));

            Matrix cur_x = x.plus(K.times(y));
            Matrix cur_P = ((Matrix.identity(4, 4).minus(K.times(H))).times(P));

            last_x = cur_x;
            last_P = cur_P;

            //System.out.println("Pos x: "+cur_x.get(0,0));
            //System.out.println("Pos y: "+cur_x.get(1,0));
            kSx += cur_x.get(0, 0);
            kSy += cur_x.get(1, 0);
            //kSz += cur_x.get(2, 0);
            kVx = cur_x.get(2,0);
            kVy = cur_x.get(3,0);
            //sLog.d(TAG, "kVx: "+kVx+" kVy: "+kVy);

        /* *****Correction of data with Kalman output for new cycle*******************************/
            //Vx = kVx;
            //Vy = kVy;
            Sx = kSx;
            Sy = kSy;

            /* ************************************************************************************/

            /* **************Converiton meter to Lat Lon *******************************************/

            //new_longitude = longitude + (dx / r_earth) * (180 / pi) / cos(latitude * pi/180);
            //new_latitude  = latitude  + (dy / r_earth) * (180 / pi);

            // 1km in degree = 1 / 111.32km = 0.0089
            // 1m in degree = 0.0089 / 1000 = 0.0000089

            //haversine formula

            tempLat = (360*(cur_x.get(1, 0)*0.001d))/CIRC_EARTH;
            latSy += tempLat;
            lonSx += (360*(cur_x.get(0, 0)*0.001d))/(CIRC_EARTH*Math.cos(tempLat));

            //Log.d(TAG,"lonSx:"+lonSx+" latSy:"+latSy);

            //Calculate total distance reached
            totSxy += Math.abs(cur_x.get(0,0)) + Math.abs(cur_x.get(1,0));

            prevLatSy = latSy;
            prevLonSx = lonSx;

            if(Math.abs(Sx) > 0.000001 && Math.abs(Sy) > 0.000001) {

                bundleData = new Bundle();
                bundleData.putDouble("Sx", lonSx);
                bundleData.putDouble("Sy", latSy);
                bundleData.putDouble("Sz", 0.0d);
                bundleData.putDouble("prevLatSy", prevLatSy);
                bundleData.putDouble("prevLonSx", prevLonSx);
                bundleData.putDouble("Ax", /*accData[0]*/Sx);
                bundleData.putDouble("Ay", /*accData[1]*/Sy);
                bundleData.putDouble("Az", stairAcc);
                //bundleData.putInt("stairCount", stairCount);
                //bundleData.putDouble("dT", acc_dT);
                bundleData.putString("marks", "0");
                bundleData.putDouble("meter", totSxy);
            }

            Az_StairDetect(stairAcc);//stair stuff

            firstZero = true;
        }else{

            //Ax = 0.0d;
            //Ay = 0.0d;
            /*VEDERE CON MOTO UNIF. DECELERATO*/

            Vx -= prevVx;
            prevVx = Vx;
            Vy -= prevVy;//0.0d;
            prevVy = Vy;

            if(firstZero) {
                zeroCount += 1;
                tempZeroCount = 1;
                firstZero = false;
            }
        }

        return bundleData;
    }

    /*
    A - state transition matrix
    B - control input matrix
    H - measurement matrix
    Q - process noise covariance matrix
    R - measurement noise covariance matrix
    P - error covariance matrix
     */

    public void initKalmanFilter(){

        Log.d(TAG, "initKalmanFilter...");
        //float err = (float)Math.sqrt(bias[0]*bias[0] + bias[1]*bias[1] + bias[2]*bias[2]);
        //0.1d
         A = new Matrix(new double[][]{

                 {1d, 0.0d, 0.7d, 0.0d},
                 {0.0d, 1.0d, 0.0d, 0.7d},
                 {0.0d, 0.0d, 1.0d, 0.0d},
                 {0.0d, 0.0d, 0.0d, 1.0d}
                 //{0d, 0d, 0d, 0d, 1d, 0d},
                 //{0d, 0d, 0d, 0d, 0d, 1d}
         });

         B = new Matrix(new double[][]{

                 {1d, 0d, 0d, 0d},
                 {0d, 1d, 0d, 0d},
                 {0d, 0d, 1d, 0d},
                 {0d, 0d, 0d, 1d},
                 //{0d, 0d, 0d, 0d, 1d, 0d},
                 //{0d, 0d, 0d, 0d, 0d, 1d},
         });

         H = new Matrix(new double[][]{
                {1.0d, 0d, 1d, 0.0d},
                {0d, 1.0d, 0d, 1d},
                {0d, 0d, 0.00001, 0d},
                {0d, 0d, 0d, 0.00001d},
                //{0d, 0d, 0d, 0d, 1d, 0d},
                //{0d, 0d, 0d, 0d, 0d, 1d},
         });

         Q = new Matrix(new double[][]{

                 {0.1d, 0d, 0d, 0d},
                 {0d, 0.1d, 0d, 0d},
                 {0d, 0d, 0.1d, 0d},
                 {0d, 0d, 0d, 0.1d},
                 //{0d, 0d, 0d, 0d, 0.0d, 0d},
                 //{0d, 0d, 0d, 0d, 0d, 0.0d}
                 //Gaussiana
                 /*
                 { Math.pow(acc_dT, 4d)/4d, 0d, Math.pow(acc_dT, 3d)/2d, 0d },
                 { 0d, Math.pow(acc_dT, 4d)/4d, 0d, Math.pow(acc_dT, 3d)/2d },
                 { Math.pow(acc_dT, 3d)/2d, 0d, Math.pow(acc_dT, 2d), 0d },
                 { 0d, Math.pow(acc_dT, 3d)/2d, 0d, Math.pow(acc_dT, 2d) }*/
         });

        R = new Matrix(new double[][]{

                {10d, 0d, 0d, 0d},
                {0d, 10d, 0d, 0d},
                {0d, 0d, 10d, 0d},
                {0d, 0d, 0d, 10d},
                //{0d, 0d, 0d, 0d, 10d, 0d},
                //{0d, 0d, 0d, 0d, 0d, 10d}

             });

        last_P = new Matrix(new double[][]{

                {0d, 0d, 0d, 0d},
                {0d, 0d, 0d, 0d},
                {0d, 0d, 0d, 0d},
                {0d, 0d, 0d, 0d},
                //{0d, 0d, 0d, 0d, 0d, 0d},
                //{0d, 0d, 0d, 0d, 0d, 0d}
        });
        //is an array
        last_x = new Matrix(new double[]{0d, 0d, 0d, 0d}, 4);

    }

    /**
     * Smoothes the signal from raw accelerometer
     */
    private double Kalman1DSmooth(double measurement){
        double f1 = mFiltersCascade[0].correct(measurement);
        double f2 = mFiltersCascade[1].correct(f1);
        double f3 = mFiltersCascade[2].correct(f2);
        return f3;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
    /*Function that calculate delta speed variation for axis x y z (now using only z)*/
    public void GyroCalculator(SensorEvent sensorEvent){

        // don't start until first accelerometer/magnetometer orientation has been acquired
        if (accMagOrientation == null)
            return;

        // initialisation of the gyroscope based rotation matrix
        if(initState) {
            double[] initMatrix;//= new float[9];
            initMatrix = getRotationMatrixFromOrientation(accMagOrientation);
            double[] test = new double[3];
            getOrientation(initMatrix, test);
            gyroMatrix = matrixMultiplication(gyroMatrix, initMatrix);
            initState = false;
        }

        // copy the new gyro values into the gyro array
        // convert the raw gyro data into a rotation vector
        double[] deltaVector = new double[4];
        if(timestamp != 0) {
            final double dT = (sensorEvent.timestamp - timestamp) * NS2S;
            //System.arraycopy(sensorEvent.values, 0, gyro, 0, 3);//copying values from sensor gyro
            for(int i = 0; i < sensorEvent.values.length; i++){

                gyro[i] = sensorEvent.values[i];
            }
            getRotationVectorFromGyro(gyro, deltaVector, dT / 2.0f);
        }
        //System.out.println("gyro z: "+ gyro[2]*dT);
        // measurement done, save current time for next interval
        timestamp = sensorEvent.timestamp;

        // convert rotation vector into rotation matrix
        double[] deltaMatrix = new double[9];
        getRotationMatrixFromVector(deltaMatrix, deltaVector);

        // User code should concatenate the delta rotation we computed with the current rotation
        // in order to get the updated rotation.
        // rotationCurrent = rotationCurrent * deltaRotationMatrix;

        // apply the new rotation interval on the gyroscope based rotation matrix
        gyroMatrix = matrixMultiplication(gyroMatrix, deltaMatrix);

        // get the gyroscope based orientation from the rotation matrix
        getOrientation(gyroMatrix, gyroOrientation);
        //return 0;
    }

    private void getRotationVectorFromGyro(double[] gyroValues, double[] deltaRotationVector,
                                           double timeFactor)
    {
        double[] normValues = new double[3];

        // Calculate the angular speed of the sample
        double omegaMagnitude =
                Math.sqrt(gyroValues[0] * gyroValues[0] +
                        gyroValues[1] * gyroValues[1] +
                        gyroValues[2] * gyroValues[2]);

        // Normalize the rotation vector if it's big enough to get the axis
        if(omegaMagnitude > EPSILON) {
            normValues[0] = gyroValues[0] / omegaMagnitude;
            normValues[1] = gyroValues[1] / omegaMagnitude;
            normValues[2] = gyroValues[2] / omegaMagnitude;
        }

        // Integrate around this axis with the angular speed by the timestep
        // in order to get a delta rotation from this sample over the timestep
        // We will convert this axis-angle representation of the delta rotation
        // into a quaternion before turning it into the rotation matrix.
        double thetaOverTwo = omegaMagnitude * timeFactor;//integrando
        double sinThetaOverTwo = Math.sin(thetaOverTwo);
        double cosThetaOverTwo = Math.cos(thetaOverTwo);
        deltaRotationVector[0] = sinThetaOverTwo * normValues[0];
        deltaRotationVector[1] = sinThetaOverTwo * normValues[1];
        deltaRotationVector[2] = sinThetaOverTwo * normValues[2];
        deltaRotationVector[3] = cosThetaOverTwo;
    }

    /*Function that calculate acceleration with magnetometer for x y */
    public void AccMagCalculator(SensorEvent sensorEvent){
        if(getRotationMatrix(rotationMatrix, null, accel, magnet)) {
            getOrientation(rotationMatrix, accMagOrientation);

        }
    }

    //Filtro complementare su tutti i sensori per giroscopio
    public class calculateFusedOrientationTask extends TimerTask {
        public void run() {
            float oneMinusCoeff = 1.0f - FILTER_COEFFICIENT;
            /*******************Fused Orientation Gyroscope plus filtering*************************/

            /*
             * Fix for 179 <--> -179 transition problem:
             * Check whether one of the two orientation angles (gyro or accMag) is negative while the other one is positive.
             * If so, add 360 (2 * math.PI) to the negative value, perform the sensor fusion, and remove the 360 from the result
             * if it is greater than 180. This stabilizes the output in positive-to-negative-transition cases.
             */

            // azimuth
            if (gyroOrientation[0] < -0.5 * Math.PI && accMagOrientation[0] > 0.0) {
                fusedOrientation[0] = (FILTER_COEFFICIENT * (gyroOrientation[0] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[0]);
                fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[0] < -0.5 * Math.PI && gyroOrientation[0] > 0.0) {
                fusedOrientation[0] = (FILTER_COEFFICIENT * gyroOrientation[0] + oneMinusCoeff * (accMagOrientation[0] + 2.0 * Math.PI));
                fusedOrientation[0] -= (fusedOrientation[0] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
                fusedOrientation[0] = FILTER_COEFFICIENT * gyroOrientation[0] + oneMinusCoeff * accMagOrientation[0];
            }

            // pitch
            if (gyroOrientation[1] < -0.5 * Math.PI && accMagOrientation[1] > 0.0) {
                fusedOrientation[1] = (FILTER_COEFFICIENT * (gyroOrientation[1] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[1]);
                fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[1] < -0.5 * Math.PI && gyroOrientation[1] > 0.0) {
                fusedOrientation[1] = (FILTER_COEFFICIENT * gyroOrientation[1] + oneMinusCoeff * (accMagOrientation[1] + 2.0 * Math.PI));
                fusedOrientation[1] -= (fusedOrientation[1] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
                fusedOrientation[1] = FILTER_COEFFICIENT * gyroOrientation[1] + oneMinusCoeff * accMagOrientation[1];
            }

            // roll
            if (gyroOrientation[2] < -0.5 * Math.PI && accMagOrientation[2] > 0.0) {
                fusedOrientation[2] = (FILTER_COEFFICIENT * (gyroOrientation[2] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[2]);
                fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[2] < -0.5 * Math.PI && gyroOrientation[2] > 0.0) {
                fusedOrientation[2] = (FILTER_COEFFICIENT * gyroOrientation[2] + oneMinusCoeff * (accMagOrientation[2] + 2.0 * Math.PI));
                fusedOrientation[2] -= (fusedOrientation[2] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
                fusedOrientation[2] = FILTER_COEFFICIENT * gyroOrientation[2] + oneMinusCoeff * accMagOrientation[2];
            }

            // overwrite gyro matrix and orientation with fused orientation
            // to compensate gyro drift
            gyroMatrix = getRotationMatrixFromOrientation(fusedOrientation);
            //System.arraycopy(fusedOrientation, 0, gyroOrientation, 0, 3);
            arrayCopy(fusedOrientation, gyroOrientation);

        }
    }

    public double[] biasCorrection(double[] accel, double bias[]) {


        double delta_x = accel[0]-bias[0];
        double delta_y = accel[1]-bias[1];
        double delta_z = accel[2]-bias[2];


        if (Math.abs(delta_x) < 0.1f) {
            bias[0] += (0.01f*delta_x);
        }
        if (Math.abs(delta_y) < 0.1f){
            bias[1] += (0.01f*delta_y);
        }
        if (Math.abs(delta_z) < 0.1f){
            bias[2] += (0.01f*delta_z);
        }

        double biasedAcc[] = new double[3];
        biasedAcc[0] = (accel[0]-bias[0]);
        biasedAcc[1] = (accel[1]-bias[1]);
        biasedAcc[2] = (accel[2]-bias[2]);

        return biasedAcc;
    }

    /* Change the device relative acceleration values to earth relative values
        X axis -> East
        Y axis -> North Pole
        Z axis -> Sky
        ENS
     */
    public double[] calculateEarthOrientedAcceleration(double acc[], double gyroData[]) {

        //last_x = new Matrix(new double[]{0d, 0d, 0d, 0d}, 4);
        Matrix deviceRelativeAcceleration = new Matrix(new double[]{
                acc[0], acc[1], acc[2]
        }, 3);

        //Rotation Matrix ZXY
        Matrix R;

        double sinX = Math.sin(gyroData[1]);
        double cosX = Math.cos(gyroData[1]);
        double sinY = Math.sin(gyroData[2]);
        double cosY = Math.cos(gyroData[2]);
        double sinZ = Math.sin(gyroData[0]);
        double cosZ = Math.cos(gyroData[0]);

        /*
            Yaw = Z
            Roll = Y
            Pitch = X
        */
        //Gimbal lock detection
        if(cosX < 0.09f){//gimbal lock condition
            //Log.d(TAG, "GimbalLock detect! : "+cosX);
            R = new Matrix(new double[][]{

                    {cosY, 0, sinX*sinZ},
                    {-sinZ, 0, sinX*cosZ},
                    {0, -sinX, 0}

            });
        }else {
            R = new Matrix(new double[][]{

                    {cosY * cosZ + sinY * sinX * sinZ,  cosX * sinZ,     -sinY * cosZ + cosY * sinX * sinZ},
                    {-cosY * sinZ + sinY * sinX * cosZ, cosX * cosZ,     sinY * sinZ + cosY * sinX * cosZ},
                    {sinY * cosX,                       -sinX,           cosY * cosX}

            });
        }
        /*
         Acceleration vector times rotation matrix
        */
        Matrix orientedAcc = R.times(deviceRelativeAcceleration);

        double temp [] = new double[]{orientedAcc.get(0,0), orientedAcc.get(1,0),
                orientedAcc.get(2,0)};

        return temp;
    }

    public double[] calculateEarthOrientedAcceleration_Quaternion(double acc[], double gyroData[]){


        double[] rotationVector = getRotationVector(gyroData);
        Quat4d qRotationVector = new Quat4d(rotationVector[0],rotationVector[1],
                rotationVector[2], rotationVector[3]);

        //System.out.println(qRotationVector.toString());
        Quat4d qAcc = new Quat4d(acc[0], acc[1], acc[2], 0 );

        //Matrix4d m = new Matrix4d(rotationVector, qAcc, 1);
        Quat4d result = new Quat4d();
        result.mul(qRotationVector, qAcc);

        double temp [] = new double[]{result.x, result.y,
                result.z};
        return temp;
    }

    public class calculateLowPassTask extends TimerTask {
        public void run() {
            float alpha = LPF_COEFFICIENT;
            float oneMinusCoeff = 1.0f - alpha;
            //******************Low Pass filter Acceleration***************************************

            //if(Sensor.TYPE_ACCELEROMETER == TYPE_ACC)

            stairAcc = alpha * accel[2]
                    + oneMinusCoeff * accel[2];

            if(linAccIsOn){

                LP_filtredAcc[0] =
                        alpha*prevFiltredAcc[0]
                                + oneMinusCoeff * androidLinearAcc[0];
                prevFiltredAcc[0] = LP_filtredAcc[0];

                LP_filtredAcc[1] =
                        alpha*prevFiltredAcc[1]
                                + oneMinusCoeff * androidLinearAcc[1];
                prevFiltredAcc[1] = LP_filtredAcc[1];

                LP_filtredAcc[2] =
                        alpha*prevFiltredAcc[2]
                                + oneMinusCoeff * androidLinearAcc[2];
                prevFiltredAcc[2] = LP_filtredAcc[2];

            }else{

                LP_filtredAcc[0] =
                        alpha*prevFiltredAcc[0]
                                + oneMinusCoeff * accel[0];
                prevFiltredAcc[0] = LP_filtredAcc[0];

                LP_filtredAcc[1] =
                        alpha*prevFiltredAcc[1]
                                + oneMinusCoeff * accel[1];
                prevFiltredAcc[1] = LP_filtredAcc[1];

                LP_filtredAcc[2] =
                        alpha*prevFiltredAcc[2]
                                + oneMinusCoeff * accel[2];
                prevFiltredAcc[2] = LP_filtredAcc[2];
            }

        }
    }


    public static boolean getRotationMatrix(double[] R, double[] I,
                                            double[] gravity, double[] geomagnetic) {
        // TODO: move this to native code for efficiency
        double Ax = gravity[0];
        double Ay = gravity[1];
        double Az = gravity[2];
        final double normsqA = (Ax * Ax + Ay * Ay + Az * Az);
        final double g = 9.81d;
        final double freeFallGravitySquared = 0.01f * g * g;
        if (normsqA < freeFallGravitySquared) {
            // gravity less than 10% of normal value
            return false;
        }
        final double Ex = geomagnetic[0];
        final double Ey = geomagnetic[1];
        final double Ez = geomagnetic[2];
        double Hx = Ey * Az - Ez * Ay;
        double Hy = Ez * Ax - Ex * Az;
        double Hz = Ex * Ay - Ey * Ax;
        final double normH = Math.sqrt(Hx * Hx + Hy * Hy + Hz * Hz);
        if (normH < 0.1f) {
            // device is close to free fall (or in space?), or close to
            // magnetic north pole. Typical values are  > 100.
            return false;
        }
        final double invH = 1.0f / normH;
        Hx *= invH;
        Hy *= invH;
        Hz *= invH;
        final double invA = 1.0f / Math.sqrt(Ax * Ax + Ay * Ay + Az * Az);
        Ax *= invA;
        Ay *= invA;
        Az *= invA;
        final double Mx = Ay * Hz - Az * Hy;
        final double My = Az * Hx - Ax * Hz;
        final double Mz = Ax * Hy - Ay * Hx;
        if (R != null) {
            if (R.length == 9) {
                R[0] = Hx;     R[1] = Hy;     R[2] = Hz;
                R[3] = Mx;     R[4] = My;     R[5] = Mz;
                R[6] = Ax;     R[7] = Ay;     R[8] = Az;
            } else if (R.length == 16) {
                R[0]  = Hx;    R[1]  = Hy;    R[2]  = Hz;   R[3]  = 0;
                R[4]  = Mx;    R[5]  = My;    R[6]  = Mz;   R[7]  = 0;
                R[8]  = Ax;    R[9]  = Ay;    R[10] = Az;   R[11] = 0;
                R[12] = 0;     R[13] = 0;     R[14] = 0;    R[15] = 1;
            }
        }
        if (I != null) {
            // compute the inclination matrix by projecting the geomagnetic
            // vector onto the Z (gravity) and X (horizontal component
            // of geomagnetic vector) axes.
            final double invE = 1.0f / Math.sqrt(Ex * Ex + Ey * Ey + Ez * Ez);
            final double c = (Ex * Mx + Ey * My + Ez * Mz) * invE;
            final double s = (Ex * Ax + Ey * Ay + Ez * Az) * invE;
            if (I.length == 9) {
                I[0] = 1;     I[1] = 0;     I[2] = 0;
                I[3] = 0;     I[4] = c;     I[5] = s;
                I[6] = 0;     I[7] = -s;     I[8] = c;
            } else if (I.length == 16) {
                I[0] = 1;     I[1] = 0;     I[2] = 0;
                I[4] = 0;     I[5] = c;     I[6] = s;
                I[8] = 0;     I[9] = -s;     I[10] = c;
                I[3] = I[7] = I[11] = I[12] = I[13] = I[14] = 0;
                I[15] = 1;
            }
        }
        return true;
    }

    public static void getRotationMatrixFromVector(double[] R, double[] rotationVector) {

        double q0;
        double q1 = rotationVector[0];
        double q2 = rotationVector[1];
        double q3 = rotationVector[2];
        if (rotationVector.length >= 4) {
            q0 = rotationVector[3];
        } else {
            q0 = 1 - q1 * q1 - q2 * q2 - q3 * q3;
            q0 = (q0 > 0) ? (float) Math.sqrt(q0) : 0;
        }
        double sq_q1 = 2 * q1 * q1;
        double sq_q2 = 2 * q2 * q2;
        double sq_q3 = 2 * q3 * q3;
        double q1_q2 = 2 * q1 * q2;
        double q3_q0 = 2 * q3 * q0;
        double q1_q3 = 2 * q1 * q3;
        double q2_q0 = 2 * q2 * q0;
        double q2_q3 = 2 * q2 * q3;
        double q1_q0 = 2 * q1 * q0;
        if (R.length == 9) {
            R[0] = 1 - sq_q2 - sq_q3;
            R[1] = q1_q2 - q3_q0;
            R[2] = q1_q3 + q2_q0;
            R[3] = q1_q2 + q3_q0;
            R[4] = 1 - sq_q1 - sq_q3;
            R[5] = q2_q3 - q1_q0;
            R[6] = q1_q3 - q2_q0;
            R[7] = q2_q3 + q1_q0;
            R[8] = 1 - sq_q1 - sq_q2;
        } else if (R.length == 16) {
            R[0] = 1 - sq_q2 - sq_q3;
            R[1] = q1_q2 - q3_q0;
            R[2] = q1_q3 + q2_q0;
            R[3] = 0.0f;
            R[4] = q1_q2 + q3_q0;
            R[5] = 1 - sq_q1 - sq_q3;
            R[6] = q2_q3 - q1_q0;
            R[7] = 0.0f;
            R[8] = q1_q3 - q2_q0;
            R[9] = q2_q3 + q1_q0;
            R[10] = 1 - sq_q1 - sq_q2;
            R[11] = 0.0f;
            R[12] = R[13] = R[14] = 0.0f;
            R[15] = 1.0f;
        }
    }

    public static double[] getOrientation(double[] R, double[] values) {
        /*
         * 4x4 (length=16) case:
         *   /  R[ 0]   R[ 1]   R[ 2]   0  \
         *   |  R[ 4]   R[ 5]   R[ 6]   0  |
         *   |  R[ 8]   R[ 9]   R[10]   0  |
         *   \      0       0       0   1  /
         *
         * 3x3 (length=9) case:
         *   /  R[ 0]   R[ 1]   R[ 2]  \
         *   |  R[ 3]   R[ 4]   R[ 5]  |
         *   \  R[ 6]   R[ 7]   R[ 8]  /
         *
         */
        if (R.length == 9) {
            values[0] = Math.atan2(R[1], R[4]);
            values[1] = Math.asin(-R[7]);
            values[2] = Math.atan2(-R[6], R[8]);
        } else {
            values[0] = Math.atan2(R[1], R[5]);
            values[1] = Math.asin(-R[9]);
            values[2] = Math.atan2(-R[8], R[10]);
        }
        return values;
    }

    private double[] getRotationMatrixFromOrientation(double[] o) {

        double[] xM = new double[9];
        double[] yM = new double[9];
        double[] zM = new double[9];

        double sinX = Math.sin(o[1]);
        double cosX = Math.cos(o[1]);
        double sinY = Math.sin(o[2]);
        double cosY = Math.cos(o[2]);
        double sinZ = Math.sin(o[0]);
        double cosZ = Math.cos(o[0]);

        /*
            Yaw = Z
            Roll = Y
            Pitch = X
         */
        /*
         *usa rotazione inversa zxy -> yxz per tirare fuori gli angoli
         * devo tornare nel frame reference del giroscopio
         */
        // rotation about x-axis (pitch)
        xM[0] = 1.0f; xM[1] = 0.0f; xM[2] = 0.0f;
        xM[3] = 0.0f; xM[4] = cosX; xM[5] = sinX;
        xM[6] = 0.0f; xM[7] = -sinX; xM[8] = cosX;

        // rotation about y-axis (roll)
        yM[0] = cosY; yM[1] = 0.0f; yM[2] = sinY;
        yM[3] = 0.0f; yM[4] = 1.0f; yM[5] = 0.0f;
        yM[6] = -sinY; yM[7] = 0.0f; yM[8] = cosY;
        /*
        yM[0] = cosY; yM[1] = 0.0f; yM[2] = -sinY;
        yM[3] = 0.0f; yM[4] = 1.0f; yM[5] = 0.0f;
        yM[6] = sinY; yM[7] = 0.0f; yM[8] = cosY;
        */

        // rotation about z-axis (azimuth)
        zM[0] = cosZ; zM[1] = sinZ; zM[2] = 0.0f;
        zM[3] = -sinZ; zM[4] = cosZ; zM[5] = 0.0f;
        zM[6] = 0.0f; zM[7] = 0.0f; zM[8] = 1.0f;

        // rotation order is z, x, y (Yaw, pitch, Roll)
        //so inverse rotation order is y, x, z (roll, pitch, azimuth) // xM yM zM
        double[] resultMatrix = matrixMultiplication(xM, yM);
        resultMatrix = matrixMultiplication(zM, resultMatrix);
        return resultMatrix;
    }

    /**
     * Create an angle-axis vector, in this case a unit quaternion, from the
     * provided Euler angle's (presumably from SensorManager.getFusedOrientation()).
     * <p>
     * Equation from
     * http://www.euclideanspace.com/maths/geometry/rotations/conversions
     * /eulerToQuaternion/
     *
     * @param orientation
     */
    protected double[] getRotationVector(double[] orientation) {
        // Assuming the angles are in radians.

        // getFusedOrientation() values:
        // values[0]: azimuth, rotation around the Z axis.
        // values[1]: pitch, rotation around the X axis.
        // values[2]: roll, rotation around the Y axis.

        // Heading, Azimuth, Yaw
        double c1 = Math.cos(orientation[0] / 2);
        double s1 = Math.sin(orientation[0] / 2);

        // Pitch, Attitude
        // The equation assumes the pitch is pointed in the opposite direction
        // of the orientation vector provided by Android, so we invert it.
        double c2 = Math.cos(-orientation[1] / 2);
        double s2 = Math.sin(-orientation[1] / 2);

        // Roll, Bank
        double c3 = Math.cos(orientation[2] / 2);
        double s3 = Math.sin(orientation[2] / 2);

        double c1c2 = c1 * c2;
        double s1s2 = s1 * s2;

        double w = c1c2 * c3 - s1s2 * s3;
        double x = c1c2 * s3 + s1s2 * c3;
        double y = s1 * c2 * c3 + c1 * s2 * s3;
        double z = c1 * s2 * c3 - s1 * c2 * s3;

        // The quaternion in the equation does not share the same coordinate
        // system as the Android gyroscope quaternion we are using. We reorder
        // it here.

        // Android X (pitch) = Equation Z (pitch)
        // Android Y (roll) = Equation X (roll)
        // Android Z (azimuth) = Equation Y (azimuth)
        double[] output =  {w, z, x, y};
        return output;
    }

    private double[] matrixMultiplication(double[] A, double[] B) {
        double[] result = new double[9];

        result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6];
        result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7];
        result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8];

        result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6];
        result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7];
        result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8];

        result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6];
        result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7];
        result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8];

        return result;
    }

    public double [] arrayCopy(double source[], double dest[] ){
        for(int i = 0; i < source.length; i++){

            dest[i] = source[i];
        }
        return dest;
    }

    public void Az_StairDetect(double Az){

        //prevMeanAz = meanMaxAz;
        //Log.d(TAG, "zeroCount"+tempZeroCount);
        stairHistory.add(new stair(tempZeroCount, Az));

        if(meanCount == NUMBER_SAMPLE){

            meanAz = AzMean;
            meanAz /= NUMBER_SAMPLE;
            //Log.d(TAG, "cusum "+cusum);
            for(int i = 0; i < stairHistory.size(); i++) {
                prevMeanAz = meanAz;
                cusum = prevMeanAz + (stairHistory.get(i).stairAcc - meanAz);

            }
            AzMean = 0;
        }

        AzMean+=Az;


        if(meanCount == NUMBER_SAMPLE) {

            for (int i = 0; i < stairHistory.size(); i++) {
                double temp = stairHistory.get(i).stairAcc - cusum;
                //Log.d(TAG,"if statement :"+temp);
                if (Math.abs(cusum - stairHistory.get(i).stairAcc) > 2.0d) {
                    stairDetect = true;
                    stairCount += stairHistory.get(i).zeroCount;

                } else
                    stairDetect = false;
            }
            stairHistory.clear();
            //cusum = 0.0d;
            meanCount = 0;
        }

        tempZeroCount = 0;
        meanCount++;
    }

    public class GUI_Update extends TimerTask {
        public void run() {
            //Return values to GUI
            //String Ax = params[0];

            if(fusedIsOn) {
                Guibuilder.append("gyro Z: " + String.format("%.3f", fusedOrientation[0]));
                Guibuilder.append("\n");
                Guibuilder.append("gyro X: " + String.format("%.3f", fusedOrientation[1]));
                Guibuilder.append("\n");
                Guibuilder.append("gyro Y: " + String.format("%.3f", fusedOrientation[2]));

                Guibuilder.append("\n");
            }else {
                Guibuilder.append("gyro Z: " + String.format("%.3f", accMagOrientation[0]));
                Guibuilder.append("\n");
                Guibuilder.append("gyro X: " + String.format("%.3f", accMagOrientation[1]));
                Guibuilder.append("\n");
                Guibuilder.append("gyro Y: " + String.format("%.3f", accMagOrientation[2]));

                Guibuilder.append("\n");
            }
            Guibuilder.append("Ax:"+String.format("%.2f", Ax)+" m/s^2 "+ "Vx:"+String.format("%.2f", Vx)+" m/s");
            Guibuilder.append("\n");
            Guibuilder.append("Ay:"+String.format("%.2f", Ay)+" m/s^2 "+ "Vy:"+String.format("%.2f", Vy)+" m/s");
            Guibuilder.append("\n");
            Guibuilder.append("isWalking: "+ isWalking + " stairCount: "+ stairCount);

            Guibuilder.append("\n");

            //builder.append("lenghtEstimation:"+String.format("%.2f", lenghtEstimation)+" m");
            //builder.append("\n");
            /*
            Guibuilder.append("Vx:"+String.format("%.2f", Vx)+" m/s");
            Guibuilder.append("\n");
            Guibuilder.append("Vy:"+String.format("%.2f", Vy)+" m/s");
            Guibuilder.append("\n");
            Guibuilder.append("Vz:"+String.format("%.2f", Vz)+" m/s");

            Guibuilder.append("\n");
            */
            Guibuilder.append("Total Meter:"+String.format("%.2f", totSxy)+" m");
            Guibuilder.append("\n");
            Guibuilder.append("zeroCount:"+zeroCount );
            Guibuilder.append("\n");
            Guibuilder.append("kSx:"+String.format("%.2f", kSx)+" m"+" kSy:"
                    +String.format("%.2f", kSy)+" m");
            //            //Guibuilder.append("kSz:"+String.format("%.2f", kSz)+" m");
            /*
            Guibuilder.append("\n");
            Guibuilder.append("isWalking: "+isWalking);
            */
            Bundle GUIbundle = new Bundle();
            GUIbundle.putString("GUI", Guibuilder.toString());
            receiver.send(GUI_UPDATE, GUIbundle);
            Guibuilder.delete(0, Guibuilder.length());
        }
    }

}