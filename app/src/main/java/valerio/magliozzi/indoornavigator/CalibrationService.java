package valerio.magliozzi.indoornavigator;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;



/**
 * Created by valerio on 22/02/18.
 */

public class CalibrationService extends Service implements SensorEventListener {

    Bundle bundle;
    ResultReceiver receiver;
    static final int STATUS_BIASING = 2;
    private static final double NS2S = 1.0f / 1000000000.0d;
    private double accTimestamp;
    private double acc_dT;
    private String TAG = "Calibration Service:";
    private SensorManager SensorMgr = null;
    private List<Integer> sensorList = new ArrayList<Integer>();
    private List<float[]> acc_samples = new ArrayList<>();
    private List<Double> dT_samples = new ArrayList<>();
    float androidLinearAcceleration[] = new float[3];
    int  count = 0, numberOfSamples = 0;
    private Timer taskTimer = new Timer();
    DecimalFormat df = new DecimalFormat("#");

    @Override
    public void onCreate(){
        // Get sensor manager on starting the service.
        SensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        df.setMaximumFractionDigits(8);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            receiver = intent.getParcelableExtra("receiver");
            Bundle extras = intent.getExtras();

            numberOfSamples = extras.getInt(("numberOfSamples"));
            Log.d(TAG, "numberOfSamples: "+numberOfSamples);


        }catch(Exception e){
            Log.d(TAG, e.toString());

        }

        sensorList.add(Sensor.TYPE_LINEAR_ACCELERATION);

        for (Integer sensor : sensorList) {
            //System.out.println("Sensor TYPE listner:"+sensor);
            SensorMgr.registerListener(this, SensorMgr.getDefaultSensor(sensor), SensorManager.SENSOR_DELAY_FASTEST, SensorManager.SENSOR_STATUS_ACCURACY_HIGH);
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d( TAG, "onDestroy" );
        SensorMgr.unregisterListener(this);
        taskTimer.cancel();
        super.onDestroy();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        switch(sensorEvent.sensor.getType()){

            case Sensor.TYPE_LINEAR_ACCELERATION:{
                System.arraycopy(sensorEvent.values, 0, androidLinearAcceleration, 0, 3);
                acc_dT = (sensorEvent.timestamp - accTimestamp) * NS2S;
                accTimestamp = sensorEvent.timestamp;
                //System.out.println("acc_dT: "+acc_dT);
                if(count >=1) {
                    dT_samples.add(acc_dT);
                }
                acc_samples.add(androidLinearAcceleration);
                count++;
                break;
            }
        }


        if (count == numberOfSamples){

            //System.out.println("CalculateBiasFromSamples:");

            bundle = new Bundle();
            bundle = CalculateBiasFromSamples(acc_samples, dT_samples, bundle);

            receiver.send(STATUS_BIASING, bundle);
            SensorMgr.unregisterListener(this, SensorMgr.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
            //taskTimer.cancel();
            return;
        }
    }

    public Bundle CalculateBiasFromSamples(List<float[]> samples, List<Double> dT_samples, Bundle bundle){

        double bias[] = new double[3];
        double bias_dT = 0;
        //Calculate sum of each samples
        for(int i = 0; i < samples.size(); i++) {

            bias[0] += samples.get(i)[0];
            bias[1] += samples.get(i)[1];
            bias[2] += samples.get(i)[2];
        }
        for(int i = 0; i < dT_samples.size(); i++){
            bias_dT += dT_samples.get(i);
        }
        for(int i = 0; i < 3; i++){
            bias[i] /= samples.size();
        }

        bias_dT /= dT_samples.size();

        bundle.putDouble("bias_x", bias[0]);
        bundle.putDouble("bias_y", bias[1]);
        bundle.putDouble("bias_z", bias[2]);
        bundle.putDouble("bias_dT", bias_dT);

        Log.d(TAG, "bias_x: "+ bias[0] + " bias_y: "+ bias[1]+
                " bias_z: "+ bias[2]);
        Log.d(TAG, "bias_dT:"+ bias_dT);
        return bundle;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
