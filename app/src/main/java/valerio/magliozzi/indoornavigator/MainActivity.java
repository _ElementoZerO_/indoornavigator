package valerio.magliozzi.indoornavigator;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static junit.framework.Assert.assertEquals;


public class MainActivity extends AppCompatActivity implements PositionReceiver.Receiver {

    Intent PositionCalculatorServiceIntent;
    Intent CalibrationServiceIntent;
    PositionReceiver myReceiver;
    TextView textView;
    ToggleButton tbStartStop, tbLinAccIsOn, tbFused_On_Off, tbLP_On_Off, tb_HPF_Acc, tb_HPF_Speed, tb_HPF_Displ;
    Button bStartCal, bSendData, bMarker;
    Spinner spin_FactorSelect;
    Spinner LPF_FactorSelect;
    private Timer taskTimer = new Timer();
    float accuracy = 0;
    //String url = "http://192.168.133.121:8080/api/file";
    int PERMISSION_ALL = 1;
    private static String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.READ_PHONE_STATE
    };
    public static final int CHECK_TIME = 30;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int INTERNET = 2;
    private static final int ACCESS_FINE_LOCATION = 3;
    private static final int ACCESS_NETWORK_STATE = 4;
    private static final int READ_PHONE_STATE = 5;

    float FILTER_COEFFICIENT;
    float LPF_COEFFICIENT;
    String TAG;
    boolean linAccIsOn = true;
    boolean fusedIsOn = true;
    boolean lowPassIsOn = true;
    boolean highPassIsOn_Acc = true, highPassIsOn_Speed = true, highPassIsOn_Displ = true;
    boolean isWalking = true, firstStop = true, isMarked = false;
    double bias[] = new double[3];
    double bias_dT = 0d;
    double prevLatSy = 0.0d, prevLonSx = 0.0d;
    private static final String DEFAULT_SEPARATOR = " ";
    DecimalFormat df = new DecimalFormat("#.######");
    public String datFile, gpxFile, pcdFile;
    double startSx, startSy;
    private ArrayList<Double> Sx = null;
    private ArrayList<Double> Sy = null;
    private ArrayList<Double> Sz = null;
    private ArrayList<Double> Ax = null;
    private ArrayList<Double> Ay = null;
    private ArrayList<Double> Az = null;
    private ArrayList<Double> meter = null;
    private ArrayList<String> marks = null;

    /* STOP TIME measurament */
    long tStart, tEnd;
    double walk_dT, elapsed_StopTime;
    /* LOCATION LISTENER****/
    //LocationManager locationManager;
    //LocationListener locationListener;

    FusedLocationProviderClient mFusedLocationClient;
    Location currentLocation = null;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 10 * 100;  /* 10 secs */
    private long FASTEST_INTERVAL = 1000; /* 2 sec */
    /* ID******/
    TelephonyManager telephonyManager;
    int parts_Counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = "MainActivity";
        System.gc();
        //df.setMaximumFractionDigits(2);
        pcdFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/indoorNav";
        datFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/indoorNav";
        gpxFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/indoorNav.gpx";
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        /* ***PERMISSIONS CHECK******/

        verifyStoragePermissions(this);
        verifyInternetPermissions(this);
        verifyFineLocationPermissions(this);
        //verifyCoarseLocationPermissions(this);
        verifyNetworkStatPermissions(this);
        verifyReadPhoneStatPermission(this);

        /* **************************/

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        /* *************NETWORK START POSITION******************/
        mFusedLocationClient = getFusedLocationProviderClient(this);
        // Acquire a reference to the system Location Manager
        //locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                currentLocation = (Location) locationResult.getLastLocation();

                accuracy = currentLocation.getAccuracy();
                //location.isFromMockProvider()
                startSx = currentLocation.getLongitude();
                startSy = currentLocation.getLatitude();
                Log.d(TAG, "Latitude: " + startSy + " Longitude: " + startSx);
                textView.setText("Start point:\n" + "Lat: " + startSy + " Lon: " + startSx + "\n Accuracy:" + accuracy);
            }
        }, Looper.myLooper());


        tbStartStop = findViewById(R.id.start_stop);
        tbLinAccIsOn = findViewById(R.id.accSelect);
        tbFused_On_Off = findViewById(R.id.fused_on_off);
        tbLP_On_Off = findViewById(R.id.lpAcc_on_off);
        tb_HPF_Acc = findViewById(R.id.id_HPF_Acc);
        tb_HPF_Speed = findViewById(R.id.id_HPF_Speed);
        tb_HPF_Displ = findViewById(R.id.id_HPF_Displ);
        bStartCal = findViewById(R.id.id_bias);
        bSendData = findViewById(R.id.id_sendBtn);
        bMarker = findViewById(R.id.btMarker);
        textView = findViewById(R.id.text);

        myReceiver = new PositionReceiver(new Handler());

        myReceiver.setReceiver(this);

        PositionCalculatorServiceIntent = new Intent(this, PositionCalculatorService.class);
        CalibrationServiceIntent = new Intent(this, CalibrationService.class);

        PositionCalculatorServiceIntent.putExtra("receiver", myReceiver);
        CalibrationServiceIntent.putExtra("receiver", myReceiver);

        spin_FactorSelect = (Spinner) findViewById(R.id.filterFactor);
        LPF_FactorSelect = (Spinner) findViewById(R.id.LPF_Factor);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.filter_factor, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.LPF_Factor, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spin_FactorSelect.setAdapter(adapter);
        LPF_FactorSelect.setAdapter(adapter2);

        spin_FactorSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                FILTER_COEFFICIENT = Float.parseFloat(spin_FactorSelect.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                FILTER_COEFFICIENT = 0.98f;
            }
        });

        LPF_FactorSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                //System.out.println("Spinner value...." + spin_FactorSelect.getSelectedItem().toString());
                LPF_COEFFICIENT = Float.parseFloat(LPF_FactorSelect.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                LPF_COEFFICIENT = 0.50f;
            }
        });

        tbLP_On_Off.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {

                    lowPassIsOn = true;
                    Log.d(TAG, "lowPassIsOn: " + lowPassIsOn);

                } else {

                    lowPassIsOn = false;
                    Log.d(TAG, "lowPassIsOn: " + lowPassIsOn);
                }
            }
        });

        tb_HPF_Acc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    highPassIsOn_Acc = true;
                    Log.d(TAG, "highPassIsOn_Acc: " + highPassIsOn_Acc);
                } else {
                    highPassIsOn_Acc = false;
                    Log.d(TAG, "highPassIsOn_Acc: " + highPassIsOn_Acc);
                }
            }
        });

        tb_HPF_Speed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    highPassIsOn_Speed = true;
                    Log.d(TAG, "highPassIsOn_Speed:" + highPassIsOn_Speed);
                } else {
                    highPassIsOn_Speed = false;
                    Log.d(TAG, "highPassIsOn_Speed:" + highPassIsOn_Speed);
                }
            }
        });

        tb_HPF_Displ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    highPassIsOn_Displ = true;
                    Log.d(TAG, "highPassIsOn_Displ:" + highPassIsOn_Displ);
                } else {
                    highPassIsOn_Displ = false;
                    Log.d(TAG, "highPassIsOn_Displ:" + highPassIsOn_Displ);

                }
            }
        });

        tbFused_On_Off.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {

                    fusedIsOn = true;
                    Log.d(TAG, "fusedIsOn: " + fusedIsOn);
                } else {

                    fusedIsOn = false;
                    Log.d(TAG, "fusedIsOn: " + fusedIsOn);
                }
            }
        });

        tbLinAccIsOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    linAccIsOn = true;//Sensor.TYPE_LINEAR_ACCELERATION;//"TYPE_LINEAR_ACCELERATION";
                    Log.d(TAG, "linAccIsOn" + linAccIsOn);
                } else {
                    linAccIsOn = false; //TYPE_ACC = Sensor.TYPE_ACCELEROMETER;
                    Log.d(TAG, "linAccIsOn" + linAccIsOn);
                }
            }
        });

        /*Start Service*/
        tbStartStop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //locationManager.removeUpdates(locationListener);
                    //correction start with previous coordinate if available
                    Log.d(TAG, "prevLatSy: "+prevLatSy + " prevLonSx: "+prevLonSx);
                    Log.d(TAG, "startSx "+startSx+ " startSy "+startSy);

                    if(prevLatSy != 0.0d && prevLonSx != 0.0d && startSx != 0.0d && startSy != 0.0d) {
                        startSx = ((startSx + prevLonSx) / 2);
                        startSy = ((startSy + prevLatSy) / 2);
                        Log.d(TAG, "startSx "+startSx+ " startSy"+startSy);
                    }
                    else if(startSx == 0.0d && startSy == 0.0d && prevLonSx !=0 && prevLatSy != 0){

                        startSx = prevLonSx;
                        startSy = prevLatSy;
                    }

                    // The toggle is enabled
                    PositionCalculatorServiceIntent.putExtra("startSx", startSx);
                    PositionCalculatorServiceIntent.putExtra("startSy", startSy);
                    PositionCalculatorServiceIntent.putExtra("linAccIsOn", linAccIsOn);
                    PositionCalculatorServiceIntent.putExtra("FILTER_COEFFICIENT", FILTER_COEFFICIENT);
                    PositionCalculatorServiceIntent.putExtra("LPF_COEFFICIENT", LPF_COEFFICIENT);
                    PositionCalculatorServiceIntent.putExtra("fusedIsOn", fusedIsOn);
                    PositionCalculatorServiceIntent.putExtra("lowPassIsOn", lowPassIsOn);
                    PositionCalculatorServiceIntent.putExtra("highPassIsOn_Acc", highPassIsOn_Acc);
                    PositionCalculatorServiceIntent.putExtra("highPassIsOn_Speed", highPassIsOn_Speed);
                    PositionCalculatorServiceIntent.putExtra("highPassIsOn_Displ", highPassIsOn_Displ);
                    PositionCalculatorServiceIntent.putExtra("bias_x", bias[0]);
                    PositionCalculatorServiceIntent.putExtra("bias_y", bias[1]);
                    PositionCalculatorServiceIntent.putExtra("bias_z", bias[2]);
                    PositionCalculatorServiceIntent.putExtra("bias_dT", bias_dT);
                    System.gc();
                    // Before doing something that requires a lot of memory,
                    // check to see whether the device is in a low memory state.
                    ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();

                    if (!memoryInfo.lowMemory) {
                        // Do memory intensive work ...
                        Sx = new ArrayList<>();
                        Sy = new ArrayList<>();
                        Sz = new ArrayList<>();
                        Ax = new ArrayList<>();
                        Ay = new ArrayList<>();
                        Az = new ArrayList<>();
                        meter = new ArrayList<>();
                        marks = new ArrayList<>();
                        Sx.clear();
                        Sy.clear();
                        Sz.clear();
                        Ax.clear();
                        Ay.clear();
                        Az.clear();
                        meter.clear();
                        marks.clear();
                        /*
                        taskTimer.scheduleAtFixedRate(new dataThresholdCheck (),
                                1000, 100);
                        */
                        startService(PositionCalculatorServiceIntent);
                    } else {
                        throw new RuntimeException("Low Memory!");
                    }
                } else {
                    // The toggle is disabled
                    /*
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission
                            (getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }else {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                    }
                    */
                    stopService(PositionCalculatorServiceIntent);
                    parts_Counter += 1;
                    startSx = 0;
                    startSy = 0;
                    writeData();
                    //writeGPX();
                    //writePCD();
                    //free space
                    System.gc();
                }

            }
        });

        bStartCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, " bStartCal.setOnClickListener");
                CalibrationServiceIntent.putExtra("numberOfSamples", 2000);
                Toast.makeText(getApplicationContext(), "Calibration started do not move the device",
                        Toast.LENGTH_LONG).show();

                startService(CalibrationServiceIntent);
            }
        });

        bMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println("marks size:"+ marks.size());
                System.out.println("Sx size:"+ Sx.size());
                marks.remove(marks.size()-1);
                marks.add("1");
                Toast.makeText(getApplicationContext(), "MARKED!",
                        Toast.LENGTH_SHORT).show();

                System.out.println("bMarker");
            }
        });

        bSendData.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Log.d(TAG, "bSendData.setOnClickListener");

                Uri uri = Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory
                        (Environment.DIRECTORY_DOWNLOADS), "indoorNav.dat"));

                System.out.println(uri);

                File fileData = new File(uri.getPath());
                //assertEquals(file.getAbsolutePath(), fileData.getAbsolutePath());
                /*
                try {
                    FileInputStream fileInputStream = new FileInputStream(fileData);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                */
                // create upload service client
                FileUploadService service =
                        ServiceGenerator.createService(FileUploadService.class);

                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"),/*multipart/form-data*/
                                fileData
                        );

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part dataFile =
                        MultipartBody.Part.createFormData("indoorNav", fileData.getName(), requestFile);

                // add another part within the multipart request
                @SuppressLint("MissingPermission") String param1 = telephonyManager.getDeviceId();
                RequestBody id_Phone =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, param1);

                String param2 = Integer.toString(parts_Counter);
                RequestBody id_Count =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, param2);

                // finally, execute the request
                Call<ResponseBody> call = service.upload(id_Phone, id_Count, dataFile);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {
                        Log.v("Upload", "success");
                        Toast.makeText(getApplicationContext(), "Data has been sended to Server",
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        Toast.makeText(getApplicationContext(), "Upload error",
                                Toast.LENGTH_LONG).show();
                    }
                });

            }

        });

    }

    // Trigger new location updates at interval
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    public void onLocationChanged(Location location) {
        // New location has now been determined
        float accuracy = location.getAccuracy();
        //location.isFromMockProvider()
        startSx = location.getLongitude();
        startSy = location.getLatitude();
        textView.setText("Start point:\n" + "Lat: " + startSy + " Lon: " + startSx + "\n Accuracy:" + accuracy);
    }

    @Override
    public void onStart() {
        super.onStart();
        taskTimer.scheduleAtFixedRate(new startCheck(),
                1000, CHECK_TIME);
        //permission checked at start time
        startLocationUpdates();
    }

    public class startCheck extends TimerTask{

        @Override
        public void run() {
            if(startSx == 0.0d && startSy == 0.0d && prevLonSx == 0 && prevLatSy == 0) {

                tbStartStop.post(new Runnable() {
                    public void run() {
                        tbStartStop.setEnabled(false);
                    }
                });

            }else{

                tbStartStop.post(new Runnable() {
                    public void run() {
                        tbStartStop.setEnabled(true);
                    }
                });
            }
        }
    }
/*
VERSION
FIELDS
SIZE
TYPE
COUNT
WIDTH
HEIGHT
VIEWPOINT
POINTS
DATA
 */
public void writePCD(){
    Log.d(TAG, "Writing PCD file...");

    Charset UTF8 = Charset.forName("UTF-8");

    StringBuilder stringTowrite = new StringBuilder();
    //parts_Counter += 1;
    Log.d(TAG, "path:"+pcdFile+parts_Counter+".pcd");

    stringTowrite.append("#.PCD v0.7 - Point Cloud Data file format\n");
    stringTowrite.append("VERSION 0.7\n");

    stringTowrite.append("FIELDS x y z\n");
    stringTowrite.append("SIZE 4 4 4\n");
    stringTowrite.append("TYPE F F F\n");
    stringTowrite.append("COUNT 1 1 1\n");
    stringTowrite.append("WIDTH "+ Sx.size()+"\n");
    stringTowrite.append("HEIGHT 1\n");
    stringTowrite.append("VIEWPOINT 0 0 0 1 0 0 0\n");
    stringTowrite.append("POINTS " + Sx.size()+"\n");
    stringTowrite.append("DATA ascii");
    stringTowrite.append("\n");

    //for(int i = 0; i < Sx.size() || i < Sy.size(); i++){
    int i = 0;
    while(i < Sx.size()){
        //stringTowrite.append("p_"+i+" ");

        stringTowrite.append(df.format(Sy.get(i)).replace(',', '.'));
        stringTowrite.append(DEFAULT_SEPARATOR);
        stringTowrite.append(df.format(Sx.get(i)).replace(',', '.'));
        stringTowrite.append(DEFAULT_SEPARATOR);
        stringTowrite.append(df.format(Sz.get(i)).replace(',', '.'));
        //Log.d(TAG, dT.get(i).toString());
        /*
           stringTowrite.append(DEFAULT_SEPARATOR);
           stringTowrite.append(Ax.get(i));
           stringTowrite.append(DEFAULT_SEPARATOR);
           stringTowrite.append(Ay.get(i));
           stringTowrite.append(DEFAULT_SEPARATOR);
           stringTowrite.append(Az.get(i));
           stringTowrite.append(DEFAULT_SEPARATOR);
           stringTowrite.append(dT.get(i));*/
        i++;

        if (i < Sx.size()) {
            stringTowrite.append("\n");
        }

    }
    //Log.d(TAG,stringTowrite.toString());
    // write on SD card file data from the text box
    try {
        File myFile = new File(pcdFile+parts_Counter+".pcd");

        if(myFile.exists()){
            myFile.delete();
        }else {
            myFile.createNewFile();
        }
        //append before it was false
        FileOutputStream fOut = new FileOutputStream(myFile, false);
        OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut, UTF8);
        myOutWriter.append(stringTowrite);
        myOutWriter.close();
        fOut.close();

    } catch (Exception e) {
        Log.e("ERR", "Could not create file",e);
    }

    Toast.makeText(getApplicationContext(),"PCD writed...",
            Toast.LENGTH_LONG).show();

}

    public void writeGPX(){
        Log.d(TAG, "Generating GPX file...");

        StringBuilder stringTowrite = new StringBuilder();
        //parts_Counter += 1;
        Log.d(TAG, "path:"+gpxFile);

        //Preable

        stringTowrite.append("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>\n" +
                "<gpx version=\"1.1\" creator=\"Indoor Navigator\" xmlns=\"http://www.topografix.com/GPX/1/1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">\n" +
                "<trk>\n" +
                "  <trkseg>");


        for(int i = 0; i < Sx.size() || i < Sy.size(); i++){

            stringTowrite.append("\n"+"    ");

            stringTowrite.append("<trkpt lat=\""+
                    df.format(Sy.get(i)).replace(',','.')+"\""+ " lon=\""+
                            df.format(Sx.get(i)).replace(',','.')+"\">"+"</trkpt>");
        }
        stringTowrite.append("\n"+"  </trkseg>\n" + "</trk>\n" + "</gpx>");
        //Log.d(TAG,stringTowrite.toString());
        // write on SD card file data from the text box
        try {
            File myFile = new File(gpxFile);

            if(myFile.exists()){
                myFile.delete();
            }else {
                myFile.createNewFile();
            }
            //append before it was false
            FileOutputStream fOut = new FileOutputStream(myFile, false);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(stringTowrite);
            myOutWriter.close();
            fOut.close();

        } catch (Exception e) {
            Log.e("ERR", "Could not create file",e);
        }

        Toast.makeText(getApplicationContext(),"GPX generated...",
                Toast.LENGTH_LONG).show();
    }

    public void writeData(){
        Log.d(TAG, "Writing dat file...");

        StringBuilder stringTowrite = new StringBuilder();
        //parts_Counter += 1;
        Log.d(TAG, "path:"+datFile);
        String prevSx = "", prevSy = "";
        double prevMeter = 0.0d;
        for(int i = 0; i < Sx.size() || i < Sy.size(); i++) {

            if (!df.format(Sx.get(i)).equals(prevSx)||!df.format(Sy.get(i)).equals(prevSy) || marks.get(i).equals("1")) {
                //Log.d(TAG, "Sx:"+Sx.get(i)+" Sy:"+Sy.get(i) );

                stringTowrite.append(df.format(Sx.get(i)).replace(',', '.'));
                stringTowrite.append(DEFAULT_SEPARATOR);
                stringTowrite.append(df.format(Sy.get(i)).replace(',', '.'));
                //----------------------------------------------------------------------------
                stringTowrite.append(DEFAULT_SEPARATOR);
                stringTowrite.append(marks.get(i));
                stringTowrite.append(DEFAULT_SEPARATOR);
                if(marks.get(i).equals("1")) {
                    stringTowrite.append(meter.get(i) - prevMeter);
                    prevMeter = meter.get(i);
                }else {
                    stringTowrite.append(meter.get(i));
                }
                stringTowrite.append(DEFAULT_SEPARATOR);
                stringTowrite.append(Ax.get(i));
                stringTowrite.append(DEFAULT_SEPARATOR);
                stringTowrite.append(Ay.get(i));

            /*
            stringTowrite.append(DEFAULT_SEPARATOR);
            stringTowrite.append(df.format(Sz.get(i)).replace(',','.'));
            //Log.d(TAG, dT.get(i).toString());
            stringTowrite.append(DEFAULT_SEPARATOR);
            stringTowrite.append(Ax.get(i));
            stringTowrite.append(DEFAULT_SEPARATOR);
            stringTowrite.append(Ay.get(i));
            stringTowrite.append(DEFAULT_SEPARATOR);
            stringTowrite.append(Az.get(i));
            stringTowrite.append(DEFAULT_SEPARATOR);
            stringTowrite.append(dT.get(i));
            */
                stringTowrite.append("\n");
            }
            prevSx = df.format(Sx.get(i));
            prevSy = df.format(Sy.get(i));
        }
        //Log.d(TAG,stringTowrite.toString());
        // write on SD card file data from the text box
        try {
            File myFile = new File(datFile +parts_Counter + ".dat" );

            if(myFile.exists()){
                myFile.delete();
            }else {
                myFile.createNewFile();
            }
            //append before it was false
            FileOutputStream fOut = new FileOutputStream(myFile, false);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(stringTowrite);
            myOutWriter.close();
            fOut.close();

        } catch (Exception e) {
            Log.e("ERR", "Could not create file",e);
        }

        Toast.makeText(getApplicationContext(),"DAT writed...",
                Toast.LENGTH_LONG).show();

    }
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {

            case PositionCalculatorService.STATUS_RUNNING: {
                Sx.add(resultData.getDouble("Sx"));
                prevLonSx = resultData.getDouble("prevLonSx");
                Sy.add(resultData.getDouble("Sy"));
                prevLatSy = resultData.getDouble("prevLatSy");
                Sz.add(resultData.getDouble("Sz"));
                //Log.d(TAG, "dT: "+resultData.getFloat("dT"));
                Ax.add(resultData.getDouble("Ax"));
                Ay.add(resultData.getDouble("Ay"));
                Az.add(resultData.getDouble("Az"));
                meter.add(resultData.getDouble("meter"));
                marks.add(resultData.getString("marks"));

                break;
            }
            case PositionCalculatorService.GUI_UPDATE: {

                textView.setText(resultData.getString("GUI").toString());
                break;
            }

            case CalibrationService.STATUS_BIASING: {
                Toast.makeText(getApplicationContext(), "Calibration Complete!",
                        Toast.LENGTH_LONG).show();
                bias[0] = resultData.getDouble("bias_x");
                bias[1] = resultData.getDouble("bias_y");
                bias[2] = resultData.getDouble("bias_z");
                bias_dT = resultData.getDouble("bias_dT");
                stopService(new Intent(getApplication(), CalibrationService.class));
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                v.vibrate(500);
                this.stopService(CalibrationServiceIntent);
                break;
            }

            case PositionCalculatorService.WALKING_UPDATE: {

                isWalking = resultData.getBoolean("isWalking");
                walk_dT = resultData.getDouble("dT");

                //Log.d("isWalking:", " "+isWalking);
                if(!isWalking) {
                    elapsed_StopTime += walk_dT;
                }

                if(!isWalking && firstStop && elapsed_StopTime > 5.0d){

                    //tbStartStop.performClick();
                    //writeData();
                    //writeGPX();
                    bSendData.performClick();
                    //tbStartStop.performClick();
                    firstStop = false;
                }

                if(isWalking) {

                    firstStop = true;
                    elapsed_StopTime = 0.0d;
                    //tStart = System.currentTimeMillis();
                }

                break;
            }

        }

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        System.gc();
        this.stopService(CalibrationServiceIntent);
    }

    // Get a MemoryInfo object for the device's current memory status.
    private ActivityManager.MemoryInfo getAvailableMemory() {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo;
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     * Bisogna sistemare per fare in modo che se non da autorizzazione l'app si chiude
     *
     * @param activity
     */


    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int memoryPermission = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (memoryPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static void verifyInternetPermissions(Activity activity) {

        int internetPermission = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.INTERNET);

        if (internetPermission != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    activity, PERMISSIONS,
                    INTERNET
            );
        }
    }

    public static void verifyFineLocationPermissions(Activity activity) {

        int accessFineLocation = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity, PERMISSIONS,
                    ACCESS_FINE_LOCATION
            );
        }
    }
    /*
    public static void verifyCoarseLocationPermissions(Activity activity) {

        int accessFineLocation = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity, PERMISSIONS,
                    accessFineLocation
            );
        }
    }*/

    public static void verifyNetworkStatPermissions(Activity activity) {

        int accessNetworkStat = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_NETWORK_STATE);
        if(accessNetworkStat != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity, PERMISSIONS,
                    ACCESS_NETWORK_STATE
            );
        }
    }

    public static void verifyReadPhoneStatPermission(Activity activity) {

        int accessReadPhoneStat = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.READ_PHONE_STATE);

        if(accessReadPhoneStat != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity, PERMISSIONS,
                    READ_PHONE_STATE
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                               String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted, yay! Do the
                        // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                        closeNow();
                }
                break;
            }
            case INTERNET: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    closeNow();
                }
                break;
            }
            case ACCESS_FINE_LOCATION: {
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Register the listener with the Location Manager to receive location updates

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    closeNow();
                }
                break;
            }

            case ACCESS_NETWORK_STATE: {
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                }else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    closeNow();
                }
                break;
            }
            case READ_PHONE_STATE: {
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                }else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    closeNow();
                }
                break;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void closeNow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            finish();
        }
    }

    public interface FileUploadService {
        @Multipart
        @POST("/api")
        Call<ResponseBody> upload(
                //@Part("key") RequestBody token,
                @Part("id_Phone") RequestBody id_Phone,
                @Part("id_Count") RequestBody id_Count,
                @Part MultipartBody.Part file
        );
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}//MainActivity

